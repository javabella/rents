$(document).ready(function() {
	if ($('#front').length > 0) {

		/**
		 * Datepickers
		 */

      	function update_datepicker(){
      	    var $dateinput = $('#front .date');
      	    $dateinput.datepicker({
      	        language: "ru",
      	        format: "yyyy-mm-dd",
      	        autoclose: true,
      	        todayHighlight: true,
      	        forceParse: true
      	    }).mask("9999-99-99");
      	}

      	update_datepicker();

      	var end = new $('#InputEndFilter').val();
      	var start = new $('#InputStartFilter').val();

      	$('#front .end-date').datepicker('setStartDate', start);
      	$('#front .start-date').datepicker('setEndDate', end);

      	$('#front .start-date').on('changeDate', function (selected) {
      	    var form = $(this).closest('form'),
      	        $end_date = form.find('.end-date'),
      	        startDate = new Date(selected.date.valueOf());
      	    $end_date.datepicker('setStartDate', startDate);
      	});

      	$('#front .end-date').on('changeDate', function (selected) {
      	    var form = $(this).closest('form'),
      	        $start_date = form.find('.start-date'),
      	        endDate = new Date(selected.date.valueOf());
      	    $start_date.datepicker('setEndDate', endDate);
      	});

      	/**
      	 * Front slider
      	 */
      	$('#front-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			fade: true,
			dots: false,
			infinite: true,
			speed: 600,
			autoplay: true,
  			autoplaySpeed: 5000
      	});
    }
});