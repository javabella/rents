/**
 * Created by pachay on 12/6/14.
 */
$(document).ready(function(){
    var $item_modal = $('#itemModal');

    var price_1day;
    var price_2days;
    var price_3to6days;
    var price_over7days;

    function set_items_hover(){
        var $itemscatalog = $('#items-catalog');

        $itemscatalog.find('.item-thumbnail').hover(
            function(){
                $(this).find('.item-caption').fadeIn(250);
            },
            function(){
                $(this).find('.item-caption').fadeOut(250);
            }
        );
    }

    $item_modal.on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget);
      var id = button.data('id');

      if (!id) {
        return;
      }

      var art = button.data('art');
      var img = button.data('img');
      var title = button.data('title');
      var min_period = button.data('min_period');
      var deposit = button.data('deposit');
      var min_period_text = '';
      var deposit_text = '';
      var min_period_title = [];

      price_1day = button.data('price');
      price_2days = button.data('price2days');
      price_3to6days = button.data('price3to6days');
      price_over7days = button.data('priceover7days');

        min_period_title[1] = 'неделя';
        min_period_title[2] = 'месяц';

      var modal = $(this);

      modal.find('.modal-body .item-title').text(title);
      modal.find('.modal-body .item-id').text("Артикул "+art);
      modal.find('.modal-body .item-img').attr("src", img);

      var end = new $('#InputEndFilter').val();
      var start = new $('#InputStartFilter').val();
      modal.find( "input[name='start']" ).val(start);
      modal.find( "input[name='end']" ).val(end);
      modal.find(".input-group.end-date").datepicker('setStartDate', start);
      modal.find(".input-group.start-date").datepicker('setEndDate', end);

      set_price();

      if (min_period && min_period != 0) {
        min_period_text = 'Минимальный период аренды - ' + min_period_title[min_period]
      }
      if (deposit != 0) {
          deposit_text = 'Залог ' + deposit + ' руб.'
      }
      modal.find('.modal-body .item-min-period').text(min_period_text);
      modal.find('.modal-body .item-deposit').text(deposit_text);
      modal.find('.modal-body .item-booking').text('');
      modal.find('.modal-body input[name="item_id"]').attr("value", id);

      $.getJSON($SCRIPT_ROOT + '/item/' + id, function( data ) {
        if (data.booking.length) {
            modal.find('.modal-body .item-booking').append($('<p>', {
                text: 'Занят в следующие даты',
                class: 'secondary'
            }));
        } else {
            modal.find('.modal-body .item-booking').append($('<p>', {
                text: 'Свободен',
                class: 'secondary'
            }));
        }

        $.each(data.booking, function(index, element) {
          modal.find('.modal-body .item-booking').append($('<p>', {
            text: element.start + ' - ' + element.end
            }));
        });
      });

    });

    $item_modal.on('hidden.bs.modal', function () {
        var form = $( "#bookingForm"),
            $end_date = form.find(".input-group.end-date"),
            $start_date = form.find(".input-group.start-date");

        form.find('input[type=text]').val('');
        form.find( 'textarea' ).val('');
        $start_date.datepicker('setEndDate', '');
        $end_date.datepicker('setStartDate', '');
        update_datepicker();

        form.find("#item-errors").text('');
        form.find(".date-period").removeClass('has-error');

    });

    $( "#bookingForm" ).submit(function( event ) {

      // Stop form from submitting normally
      event.preventDefault();

      // Get some values from elements on the page:
      var $form = $( this ),
        item_id = $form.find( "input[name='item_id']" ).val(),
        start = $form.find( "input[name='start']" ).val(),
        end = $form.find( "input[name='end']" ).val(),
        comment = $form.find( "textarea[name='comment']" ).val(),
        url = $form.attr( "action" );

      // Send the data using post
      var posting = $.post( url, { item_id: item_id, start: start, end: end, comment: comment } );

      // Put the results in a div
      posting.done(function( data ) {
        $('#itemModal').modal('hide');
        window.location.href = $SCRIPT_ROOT + "/cart";
      });

      posting.fail(function( jqXHR, textStatus ) {
        $form.find("#item-errors").text(jqXHR.responseText);
        $form.find(".date-period").addClass('has-error');
      });
    });

    function update_datepicker(){
        var $dateinput = $('.input-group.date');
        $dateinput.datepicker({
            language: "ru",
            format: 'yyyy-mm-dd',
            autoclose: true,
            todayHighlight: true,
            forceParse: true
        }).mask("9999-99-99");
    }

    update_datepicker();

    $('input.phone_number').mask("+7 (999) 999-99-99");

    $(function() {
        $(".input-group.start-date").on('changeDate', function (selected) {
            var form = $(this).closest("form"),
                $end_date = form.find(".input-group.end-date"),
                startDate = new Date(selected.date.valueOf());
            $end_date.datepicker('setStartDate', startDate);
            });

        $(".input-group.end-date").on('changeDate', function (selected) {
            var form = $(this).closest("form"),
                $start_date = form.find(".input-group.start-date"),
                endDate = new Date(selected.date.valueOf());
            $start_date.datepicker('setEndDate', endDate);
            });

        var form = $('form.filter');
        var container = $('#items-container'),
            $end_date = form.find(".input-group.end-date"),
            $start_date = form.find(".input-group.start-date");
        form.find("#show_all").click(function( event ) {
            event.preventDefault();
            form.find('select').val('0');
            form.find('input[type=checkbox]').attr('checked', false);
            form.find('input[type=text]').val('');
            $start_date.datepicker('setEndDate', '');
            $end_date.datepicker('setStartDate', '');
            update_datepicker();

            $.ajax({
                type: "GET",
                url: form.action,
                success: function(msg){
                    container.html(msg);
                    set_items_hover();
                },
                error: function(){
                    console.log("filter failure");
                }
            });
        });
        form.find("#filter_btn").click(function( event ) {
            event.preventDefault();
            $.ajax({
                type: "GET",
                url: form.action,
                data: form.serialize(),
                success: function(msg){
                    container.html(msg);
                    set_items_hover();
                },
                error: function(){
                    console.log("filter failure");
                }
            });
        });
        // form.find("input[type=checkbox]").change(function(){
        //     $.ajax({
        //     type: "GET",
        //     url: form.action,
        //     data: form.serialize(),
        //     success: function(msg){
        //         container.html(msg);
        //         set_items_hover();
        //         },
        //     error: function(){
        //         console.log("filter failure");
        //         }
        //     });
        // });
        // form.find(".input-group.date").on('changeDate', function (ev) {
        //     $.ajax({
        //     type: "GET",
        //     url: form.action,
        //     data: form.serialize(),
        //     success: function(msg){
        //         container.html(msg);
        //         set_items_hover();
        //         },
        //     error: function(){
        //         console.log("filter failure");
        //         }
        //     });
        // });
    });

    $(".clickable").click(function() {
        window.location = $(this).data("location");
        return false;
    });

    set_items_hover();
    
    $(".input-group.date").on('changeDate', function () {
        set_price();
    });

    function set_price() {
        var $begin = $("#InputStart").val();
        var $end = $("#InputEnd").val();
        var price = price_1day;
        if ($begin.length && $end.length) {
            var begin_date = new Date($begin);
            var end_date = new Date($end);
            var days = Math.floor((end_date - begin_date) / (1000*60*60*24));
            if (days >= 7 && price_over7days > 0) {
                price = price_over7days;
            } else if (days >= 3 && price_3to6days > 0) {
                price = price_3to6days;
            } else if (days >= 2 && price_2days > 0) {
                price = price_2days;
            }
        }
        $item_modal.find('.modal-body .item-price').text(price + ' руб. за день');
    }

});
